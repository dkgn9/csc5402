# -*- coding: utf-8 -*-
"""
Created on Tue Sep 18 22:02:22 2018

@author: HP
"""

import pandas as pd
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import pydotplus
import io  
from IPython.display import Image  

train_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\train.4.1.csv')
test_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\test.4.1.csv') 

print(train_df['Label: Win/Lose'])
train_df['Is Home/Away?'] = train_df['Is Home/Away?'].map({'Home': 1, 'Away': 0})
train_df['Is Opponent in AP Top 25 at Preseason?'] = train_df['Is Opponent in AP Top 25 at Preseason?'].map({'Out': 1, 'In': 0})
train_df['Media'] = train_df['Media'].map({'4-ABC':3,'3-FOX':2,'2-ESPN': 1, '1-NBC': 0})
train_df['Label: Win/Lose'] = train_df['Label: Win/Lose'].map({'Win':1,'Lose':0})
print(train_df)

test_df['Is Home/Away?'] = test_df['Is Home/Away?'].map({'Home': 1, 'Away': 0})
test_df['Is Opponent in AP Top 25 at Preseason?'] = test_df['Is Opponent in AP Top 25 at Preseason?'].map({'Out': 1, 'In': 0})
test_df['Media'] = test_df['Media'].map({'4-ABC':3,'3-FOX':2,'2-ESPN': 1, '1-NBC': 0})
test_df['Label: Win/Lose'] = test_df['Label: Win/Lose'].map({'Win':1,'Lose':0})
print(test_df)

train_df = train_df.drop(columns=['Id','University','Date'])
test_df = test_df.drop(columns=['Id','University','Date'])
combine = [train_df,test_df]
balance_data = pd.concat(combine,sort=False)

X = balance_data.values[:,0:3]
Y = balance_data.values[:,-1]
X_train, X_test, y_train,y_test = train_test_split( X, Y, test_size = 0.8)
#
clf = tree.DecisionTreeClassifier(criterion = "entropy",
                               max_depth=None, min_samples_leaf=5)

clf=clf.fit(X_train,y_train)
print(clf)
pred = clf.predict(X_test)
print("exptected values",pred)
print("accuracy:", accuracy_score(y_test,pred)*100)
print("Confusion Matrix:",confusion_matrix(y_test,pred))

dot_data = io.StringIO()

tree.export_graphviz(clf, out_file=dot_data,  
                filled=True, rounded=True,
                special_characters=True)
#print(dot_data.getvalue())
graph = pydotplus.graphviz.graph_from_dot_data(dot_data.getvalue())  
plt = Image(graph.create_png())
display(plt)







