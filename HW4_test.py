# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 20:28:43 2018

@author: djsur
"""
import pandas as pd
from sklearn.metrics import accuracy_score

def pred(model,tree_model,test):
    df = pd.DataFrame(test)
    X = df.values[:,0:5]
    Y = df.values[:,-1]
    predicted_values = model.predict(X)
    acc = accuracy_score(predicted_values,Y)
    
    predicted_values_tree = tree_model.predict(X)
    acc_tree = accuracy_score(predicted_values_tree,Y)
    return (acc,acc_tree)