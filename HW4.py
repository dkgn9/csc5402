# -*- coding: utf-8 -*-
"""
Created on Tue Oct  2 23:06:24 2018

@author: djsur
"""

import pandas as pd
import numpy as np
from sklearn.model_selection import KFold
import HW4_train,HW4_test
import matplotlib.pyplot as plt


iris = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW4\iris_data.csv')
df = np.array(iris.to_records())

kd = {}
k=1
for k in range(1,100,30):
    
    kf = KFold(n_splits=5,shuffle=True)
    accuracy_knn = []
    accuracy_dt = []
    for train_idx, test_idx in kf.split(df):
        train, test = df[train_idx], df[test_idx]
        model,tree_model = HW4_train.learn_data_set(train,k)
        acc,acc_tree = HW4_test.pred(model,tree_model,test)
        accuracy_knn.append(acc)
        accuracy_dt.append(acc_tree)
    kd[k] = np.mean(accuracy_knn)
 
print("Accuracy values of 5 folds using knn :",kd)
print("Accuracy values of 5 folds using Decision tree :",accuracy_dt)

mu, sigma = 100, 15
x = mu + sigma * np.random.randn(10000)

bins, edges = np.histogram(x, 50, normed=1)
left,right = edges[:-1],edges[1:]
X = np.array([left,right]).T.flatten()
Y = np.array([bins,bins]).T.flatten()

plt.plot(kd.values(),kd.keys())
plt.show()

plt.bar(kd.values(),kd.keys())
plt.show()