 
import pandas as pd
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import average_precision_score
import pydotplus
import io  
from IPython.display import Image  

train_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\train.5.csv')
test_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\test.5.csv') 

print(train_df['Media'])
train_df['Is_Home_or_Away'] = train_df['Is_Home_or_Away'].map({'Home': 1, 'Away': 0})
train_df['Is_Opponent_in_AP25_Preseason'] = train_df['Is_Opponent_in_AP25_Preseason'].map({'Out': 1, 'In': 0})
train_df['Media'] = train_df['Media'].map({'5-CBS':4,'4-ABC':3,'3-FOX':2,'2-ESPN': 1, '1-NBC': 0})
train_df['Label'] = train_df['Label'].map({'Win':1,'Lose':0})
#print(train_df)

test_df['Is_Home_or_Away'] = test_df['Is_Home_or_Away'].map({'Home': 1, 'Away': 0})
test_df['Is_Opponent_in_AP25_Preseason'] = test_df['Is_Opponent_in_AP25_Preseason'].map({'Out': 1, 'In': 0})
test_df['Media'] = test_df['Media'].map({'5-CBS':4,'4-ABC':3,'3-FOX':2,'2-ESPN': 1, '1-NBC': 0})
test_df['Label'] = test_df['Label'].map({'Win':1,'Lose':0})

train_df = train_df.drop(columns=['ID','Opponent','Date'])
test_df = test_df.drop(columns=['ID','Opponent','Date'])
combine = [train_df,test_df]
balance_data = pd.concat(combine,sort=False)

X = balance_data.values[:,0:3]
Y = balance_data.values[:,-1]
X_train, X_test, y_train,y_test = train_test_split( X, Y, test_size = 0.33)

clf = GaussianNB()

clf=clf.fit(X_train,y_train)
print(clf)
pred = clf.predict(X_test)
print("exptected values",pred)
print("accuracy:", accuracy_score(y_test,pred)*100)
print("Confusion Matrix:",confusion_matrix(y_test,pred))
print("Recall score:",recall_score(y_test,pred))
print("F1 score:",f1_score(y_test,pred))
print("Aveage precision score:",average_precision_score(y_test,pred))

