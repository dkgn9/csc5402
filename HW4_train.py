# -*- coding: utf-8 -*-
"""
Created on Wed Oct  3 19:41:38 2018

@author: djsur
"""

import pandas as pd
from sklearn import neighbors
from sklearn import tree
from sklearn.naive_bayes import GaussianNB

def learn_data_set(train,i):
    print("neighbor:",i)
    df = pd.DataFrame(train)
    X = df.values[:,0:5]
    Y = df.values[:,-1]
    clf = neighbors.KNeighborsClassifier(n_neighbors=i)
    clf.fit(X,Y)
    
    dlf = GaussianNB()
    dlf.fit(X,Y)
    return (clf,dlf)    