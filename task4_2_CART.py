# -*- coding: utf-8 -*-
"""
Created on Wed Sep 19 21:06:53 2018

@author: HP
"""

import pandas as pd
from sklearn import tree
from sklearn.cross_validation import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import pydotplus
import io  
from IPython.display import Image

train_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\train.4.2.csv')
test_df = pd.read_csv(r'C:\Dhananjay\writing\USA\MST\MST_FS2018\COMP_SCI 5402 INTRO TO DATA MINING (LEC 1A) FS2018\HW\HW2\test.4.2.csv') 

train_df['Outlook'] = train_df['Outlook'].map({'Sunny': 2, 'Overcast': 1,'Rainy':0})
train_df['Temperature'] = train_df['Temperature'].map({'Hot': 2, 'Mild': 1, 'Cool':0})
train_df['Humidity'] = train_df['Humidity'].map({'High':1,'Normal':0})
train_df['Windy'] = train_df['Windy'].astype(int)
train_df['Label: Play?'] = train_df['Label: Play?'].map({'Yes':1,'No':0})


test_df['Outlook'] = test_df['Outlook'].map({'Sunny': 2, 'Overcast': 1,'Rainy':0})
test_df['Temperature'] = test_df['Temperature'].map({'Hot': 2, 'Mild': 1, 'Cool':0})
test_df['Humidity'] = test_df['Humidity'].map({'High':1,'Normal':0})
test_df['Windy'] = test_df['Windy'].astype(int)
test_df['Label: Play?'] = test_df['Label: Play?'].map({'Yes':1,'No':0})


train_df = train_df.drop(columns=['ID','Date'])
test_df = test_df.drop(columns=['ID','Date'])
combine = [train_df,test_df]
balance_data = pd.concat(combine,sort=False)

X = balance_data.values[:,0:4]
Y = balance_data.values[:,-1]
X_train, X_test, y_train,y_test = train_test_split( X, Y, test_size = 0.1)
#
clf = tree.DecisionTreeClassifier(criterion = "gini",
                               max_depth=None)

clf=clf.fit(X_train,y_train)
print(clf)
pred = clf.predict(X_test)
print("exptected values",pred)
print("accuracy:", accuracy_score(y_test,pred)*100)
print("Confusion Matrix:",confusion_matrix(y_test,pred))

dot_data = io.StringIO()

tree.export_graphviz(clf, out_file=dot_data,  
                filled=True, rounded=True,
                special_characters=True)
graph = pydotplus.graphviz.graph_from_dot_data(dot_data.getvalue())  
plt = Image(graph.create_png())
display(plt)







